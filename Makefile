build: Cleaner Tests
	dotnet build

run: Cleaner
	dotnet build Cleaner
	dotnet run --project Cleaner

test: Cleaner Tests
	dotnet test
