## Dependencies
dotnet CLI

## Tests
```make test```

## Build
```make build```

## Run
```make run```
then follow the instructions

NOTES::
When a bad input is given, we ignore it but we still decrement the number of
remaining instructions. We consider the inputs to be correctly parsed so the
bad input is either a wrong direction (not in { N, S, E, W }) or a move that
takes the robot out of bounds. 
We do not output any error message so there is no way to know if an
instruction was actually taken into account.
