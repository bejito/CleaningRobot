﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many instructions shall I execute?");
            int numInstructions  = int.Parse(Console.ReadLine());

            Console.WriteLine("Where should I start from ?");
            string[] coord = Console.ReadLine().Split(' ');
            int x = int.Parse(coord[0]);
            int y = int.Parse(coord[1]);

            var robot = new Robot(x, y);


            for (int i = 0; i < numInstructions; i++) {
                Console.WriteLine("Enter step {0}", i + 1);
                string step = Console.ReadLine();
                
                try 
                {
                    robot.Move(step);
                }

                catch (System.ArgumentException e)
                {
                    // Console.Error.WriteLine(e.Message);
                }
            }

            Console.WriteLine("=> Cleaned: {0}", robot.GetCount());

        }
    }
}
