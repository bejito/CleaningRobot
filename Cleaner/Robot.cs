using System;
using System.Collections.Generic;
using System.Linq;


namespace Cleaner {
    public enum Direction { 
        E = 'E', 
        W = 'W', 
        N = 'N', 
        S = 'S'
    };

    public class Robot {
        private int x, y;
        private Dictionary<long, bool> office;

        public const int OFFICE_SIZE = 100000;

        public Robot(int x, int y) {
            this.x = x;
            this.y = y;
            this.office = new Dictionary<long, bool>();
            this.office.Add(getKey(x, y), true);
        }

        static long getKey(int x, int y) 
        {
            // Create a key from the coordinates
            // Since x can be up to 100000 > 2^16 we need long
            // Also checks that the keys is in the bounds
            if (Math.Abs(x) > OFFICE_SIZE) {
                throw new System.ArgumentException(
                    "You can't send the robot out of the office", "coord x"
                );
            }
            if (Math.Abs(y) > OFFICE_SIZE) {
                throw new System.ArgumentException(
                    "You can't send the robot out of the office", "coord y"
                );
            }

            return ((long)x << 32) + (long)y;
        }

        public int GetCount()
        {
            return this.office.Keys.Count();
        }

        public void Move(string step) {
            string[] move = step.Split(' ');
            int n = int.Parse(move[1]);

            switch(Enum.Parse(typeof(Direction), move[0]))
            {
                case Direction.N:
                    this.MoveN(n);
                    return;
                case Direction.S:
                    this.MoveS(n);
                    return;
                case Direction.E:
                    this.MoveE(n);
                    return;
                case Direction.W:
                    this.MoveW(n);
                    return;
                default:
                    throw new System.ArgumentException("Direction not recognized", move[0]);
            }
        }

        void MoveN(int n) {
            // Going to north <=> increasing y
            for (int i = 1; i < n + 1; i++) {
                this.office[getKey(x, y + i)] = true;
            }

            this.y = y + n;
        }

        void MoveS(int n) {
            // Going to north <=> decreasing y
            for (int i = 1; i < n + 1; i++) {
                this.office[getKey(x, y - i)] = true;
            }

            this.y = y - n;
        }

        void MoveE(int n) {
            // Going to north <=> increasing x
            for (int i = 1; i < n + 1; i++) {
                this.office[getKey(x + i, y)] = true;
            }

            this.x = x + n;
        }

        void MoveW(int n) {
            // Going to north <=> decreasing x
            for (int i = 1; i < n + 1; i++) {
                this.office[getKey(x - i, y)] = true;
            }

            this.x = x - n;
        }

    }
}
