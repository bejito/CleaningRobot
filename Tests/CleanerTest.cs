using System;
using Xunit;
using Cleaner;

namespace Tests
{
    public class CleanerTests
    {

        [Fact]
        public void NoMove()
        {
            var robot = new Robot(0, 0);
            Assert.Equal(1, robot.GetCount());
        }

        [Fact]
        public void MoveOneWay()
        {
            var robot = new Robot(0, 0);
            robot.Move("N 4");

            Assert.Equal(5, robot.GetCount());
        }

        [Fact]
        public void MoveBackAndForth()
        {
            var robot = new Robot(0, 0);
            robot.Move("N 4");
            robot.Move("S 2");

            Assert.Equal(5, robot.GetCount());
        }
        
        [Fact]
        public void MultiplePassageOnSameTile()
        {
            var robot = new Robot(0, 0);
            robot.Move("N 4");
            robot.Move("E 2");
            robot.Move("S 1");
            robot.Move("W 4");

            Assert.Equal(11, robot.GetCount());
        }

        [Fact]
        public void WrongDirection()
        {
            var robot = new Robot(0, 0);

            Assert.Throws<System.ArgumentException>(delegate { robot.Move("X 4"); });
        }
        
        [Fact]
        public void MoveOutOfBounds()
        {
            Robot robot;

            // x > 100000
            robot = new Robot(99999, 0);
            Assert.Throws<System.ArgumentException>(delegate { robot.Move("E 4"); });

            // x < -100000
            robot = new Robot(-99999, 0);
            Assert.Throws<System.ArgumentException>(delegate { robot.Move("W 4"); });

            // y > 100000
            robot = new Robot(0, 99999);
            Assert.Throws<System.ArgumentException>(delegate { robot.Move("N 4"); });

            // y < -100000
            robot = new Robot(0, -99999);
            Assert.Throws<System.ArgumentException>(delegate { robot.Move("S 4"); });
        }

        [Fact]
        public void CountMovesUntilBound()
        {

            var robot = new Robot(99995, 0);
            Assert.Throws<System.ArgumentException>(delegate { robot.Move("E 10"); });
            Assert.Equal(6, robot.GetCount());
        }
    }
}
